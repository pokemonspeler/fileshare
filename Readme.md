# Simple File share web tool

in `private/files.php` is an array with the format of a key pointing at an array with the versions of the file. where the version points at the title for the file and the extention of the file. see code block below for an example
note: the first key is equal to the name of the file

```php
const DATA = [
    "space" => [
        "v1" => [
            "type" => "jpg",
            "title" => "caleb-shong-1-sHHGjZDXY-unsplash",
        ],
    ],
];
```

## Webserver configuration
Point apache or nginx or whatever hosting sytem at the `/web` folder

## retrieving file

based upon the codeblock up hear the url for retreaving your file woul be as follow: `http://yourdomain.com?file=space&version=v1`

## copyrights
space.jpg => https://unsplash.com/photos/1-sHHGjZDXY
