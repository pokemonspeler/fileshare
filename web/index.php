<?php
require_once "../private/files.php";


if (isset($_REQUEST["file"]) && isset($_REQUEST["version"])) {
    // Get parameters
    $file = urldecode($_REQUEST["file"]); // Decode URL-encoded string
    $version = urldecode($_REQUEST["version"]); // Decode URL-encoded string

    /* Test whether the file name contains illegal characters
    such as "../" using the regular expression */
    if (key_exists($file, DATA) && key_exists($version, DATA[$file])) {
        $filepath = "../src/" . $file . "." . $version . "." . DATA[$file][$version]["type"];

        // Process download
        if (file_exists($filepath)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . DATA[$file][$version]["title"] . "." . DATA[$file][$version]["type"] . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filepath));
            flush(); // Flush system output buffer
            readfile($filepath);
        } else {
            http_response_code(404);
        }
        die();
    } else {
        die("Invalid file name!");
    }
}
